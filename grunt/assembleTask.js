const config = require('../config.js');

module.exports = {
  options: {
    layout: 'default.html',
    layoutdir: 'src/layouts/',
    partials: 'src/partials/*.html',
    flatten: true
  },
  site: {
    src: 'src/pages/index.html',
    dest: config.dist
  }
};
