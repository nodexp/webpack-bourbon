
const src = 'src';
const dist = '.dist';

module.exports = {
  src,
  dist,
  app: {

    /**
     * The location of the index.html for the app. It will be copied into the
     * distribution folder. See `grunt/copyTask.js`
     */
    'index.html': `${src}/index.html`,

    /**
     * The entry point for the webpack bundler.
     */
    'webpack-entry-point': `${src}/app.js`,

    /**
     * css files that should be copied "as is" to the destination. See
     * `grunt/copyTask`
     */
    'styles': {
      sources: `${src}/css/**/*.css`,
      dest: `${dist}/css`
    },

    /**
     * images that should be copied to the destination. See
     * `grunt/copyTask`
     */
    'images': {
      sources: `${src}/assets/img/**/*`,
      dest: `${dist}/assets/img`
    }
  },

  /**
   * Where the webpack bundle file will be saved.
   */
  bundleName: `${dist}/bundle.js`,

  /**
   *  third party stuff
   */
  vendor: {

    styles: [/* TODO */],

    scripts: {

      /**
       * destination for third party script files
       */
      dest: `${dist}/vendor/js/`,

      /**
       * third party script sources. All this files will be copied to
       * `vendor.scripts.dest` folder.
       */
      sources: [
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery/dist/jquery.js',
        // 'node_modules/foundation-sites/js/foundation.*.js',
        'node_modules/particles.js/particles.js',
      ]
    },

    fonts: [/* TODO */]
  },
  server: {
    port: 3003
  }
};
