const config = require('./config.js');
const bourbonPaths = require('node-bourbon').includePaths;
const neatPaths = require('node-neat').includePaths;

const allPaths = bourbonPaths.concat(neatPaths);
const sassPaths = allPaths.map((sassPath) => `includePaths[]=${sassPath}`).join('&');

/**
 * Webpack's configuration file.
 */
module.exports = {
  entry: `./${config.app['webpack-entry-point']}`,
  output: {
    filename: config.bundleName
  },

  // Enable source-maps for debugging webpack's output.
  devtool: 'source-map',

  resolve: {
    // Add '.js' and '.jsx' as resolvable extensions.
    extensions: ['', '.json', '.js', '.jsx', '.scss', '.sass']
  },

  module: {
    loaders: [

      /**
       * The `exports-loader` allow us to require() modules that lack of
       * exports (non commonJS modules).
       *
       * See https://www.npmjs.com/package/exports-loader
       */
      {
        test: /particles\.js?$/,
        loader: 'exports',
        query: {
          'particlesJS': 'window.particlesJS'
        }
      },

      {
        test: /scrollmagic\/uncompressed\/plugins/,
        loader: 'imports?define=>false'
      },

      {
        test: /slick\.js$/,
        loader: 'imports?define=>false'
      },

      /**
       * JS files will be handled by 'babel-loader'.
       */
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        cacheDirectory: './wpcache',
        query: {
          presets: ['es2015']
        }
      },


      {
        test: /\.json?$/,
        loader: 'json-loader'
      },

      /**
       * CSS files will be handled by 'css' & `style` loaders.
       * NOTE: The resulting css will be injected in the webpack output bunlde
       * javascript file. Then, it will be injected in your document via
       * <style> tag.
       */
      {
        test: /\.css$/,
        loader: 'style!css?sourceMap'

      },

      {
        test: /\.scss$/,
        loaders: ['style', 'css?sourceMap', `sass?sourceMap?${sassPaths}`]
      },

      {
        test: /\.sass$/,
        loaders: ['style', 'css?sourceMap', `sass?sourceMap?${sassPaths}`]
      }
    ]
  },

  externals: {
    'jquery': '$'
  }
};
