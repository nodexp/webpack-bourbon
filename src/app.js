const particlesJS = require('particles.js').particlesJS;
const ScrollMagic = require('scrollmagic');

require('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js');
require('scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js');

require('slick-carousel');
require('jquery.easing');
require('jquery.easypin/dist/jquery.easypin.js');

require('foundation-sites');

// require('./assets/scss/app.scss');

$(document).foundation();

$(function () {
    /*
     * --------------------------------------------------------------
     * BEGIN ScrollMagic implementation.
     * DOCS: http://scrollmagic.io/docs/index.html
     *
     * HOW TO para animaciones:
     * https://github.com/janpaepke/ScrollMagic/wiki/Get-Started-Setup
     * https://github.com/janpaepke/ScrollMagic/wiki/Get-Started-First-Tween
     *
     * Uitliza GSAP para animaciones (TweenMax, TweenlineMax, etc.). Ver:
     * http://scrollmagic.io/docs/animation.GSAP.html#Scene.setTween
     * http://greensock.com/docs/#/HTML5/
     * http://greensock.com/docs/#/HTML5/GSAP/TweenMax/
     */

    // CONTROLADORES
    // =============
    // Controller principal que maneja el scroll completo del sitio.
    // El menú debería usar este controller para dirigirse a las distinas secciones.
    // Ver http://scrollmagic.io/docs/ScrollMagic.Controller.html#scrollTo
    var mainController = new ScrollMagic.Controller();
    // ESCENAS
    // =======
    // Animación para el primer título.
    // --------------------------------
    var makingItHappenTween = new TweenMax.to('#making-it-happen', 2, {
        opacity: '1.0',
        left: '10%'
    });

    var makingItHappenScene = new ScrollMagic.Scene({
        offset: 0 // start immediately
    })
        .setTween(makingItHappenTween)
        .addIndicators()
        .addTo(mainController);

    // Texto quiénes somos
    // -------------------
    var whoWeAreTextTween = new TweenMax.to('#who-we-are-text', .3, {
        opacity: '1.0',
        marginTop: '-6em'
    });
    var whoWeAreTextScene = new ScrollMagic.Scene({
        offset: '120%'
    })
        .setTween(whoWeAreTextTween)
        .tweenChanges(true)
        .addIndicators()
        .addTo(mainController);

    // Our values
    // ----------
    var ourValuesScene = new ScrollMagic.Scene({
        triggerElement: 'section.our-values'
    });

    var ourValuesTimeline = new TimelineMax();
    var ourValuesTitleTween = new TweenMax.to('#our-values-title h2', .5, {
        opacity: '1.0'
    });
    ourValuesTimeline.add(ourValuesTitleTween);
    $('#values-container .value').each(function (i, obj) {
        var $obj = $(obj);
        var tweenProperties = {
            delay: 1,
            opacity: 0.01
        };
        if ($obj.hasClass('even')) {
            tweenProperties.right = '-80%';
        } else {
            tweenProperties.left = '-80%';
        }
        var tween = new TweenMax.from($obj, .2, tweenProperties);
        ourValuesTimeline.add(tween);
    });
    ourValuesScene
        .setTween(ourValuesTimeline)
        .addIndicators()
        .tweenChanges(true)
        .addTo(mainController);

    // Our work
    // ----------
    var ourWorkScene = new ScrollMagic.Scene({
        triggerElement: 'section.work'
    });

    var ourWorkTimeline = new TimelineMax();
    var ourWorkTitleTween = new TweenMax.to('#work-title h2', .5, {
        opacity: '1.0'
    });
    ourWorkTimeline.add(ourWorkTitleTween);
    ourWorkScene
        .setTween(ourWorkTimeline)
        .addIndicators()
        .tweenChanges(true)
        .addTo(mainController);

    // Stats
    // ---------

    var statsScene = new ScrollMagic.Scene({
        triggerElement: "section.stats #stats-container"
    });
    var statsTimeline =  new TimelineMax();

    var statsTitleTween = new TweenMax.to('#stats-title h2', .5, {
        opacity: '1.0'
    });

    statsTimeline.add(statsTitleTween);
    statsScene
        .setTween(statsTimeline)
        .addIndicators()
        .tweenChanges(true)
        .addTo(mainController);


    // Download app
    // -------------------
    var statslinksTween = new TweenMax.to('#stats-links', .23, {
        opacity: '1.0',
        top: '5%',  /* position the top  edge of the element at the middle of the parent */
        left: '0%' /* position the left edge of the element at the middle of the parent */

    });
    var statslinksScene = new ScrollMagic.Scene({
        /*offset: '40%'*/
    })
        .setTween(statslinksTween)
        .tweenChanges(true)
        .addIndicators()
        .addTo(mainController);


     // Team
     // ------

    var teamScene = new ScrollMagic.Scene({
        triggerElement: "section.team #team-container"
    });
    var teamTimeline =  new TimelineMax();

    var teamTitleTween = new TweenMax.to('#team-title h2', .5, {
        opacity: '1.0'
    });

    teamTimeline.add(teamTitleTween);
    teamScene
        .setTween(teamTimeline)
        .addIndicators()
        .tweenChanges(true)
        .addTo(mainController);


   // Contact
   // ------

    var contactScene = new ScrollMagic.Scene({
        triggerElement: "section.contact"
    });
    var contactTimeline =  new TimelineMax();

    var contactTitleTween = new TweenMax.to('#contact-title h2', .5, {
        opacity: '1.0'
    });

    contactTimeline.add(contactTitleTween);
    contactScene
        .setTween(contactTimeline)
        .addIndicators()
        .tweenChanges(true)
        .addTo(mainController);



  //  scroll con los anchor de Nav

  $(document).on("click", "#navegacion a[href^='#']", function (e) {
    var id = $(this).attr("href");

    if ($(id).length > 0) {
      e.preventDefault();
      mainController.scrollTo(id);
      if (window.history && window.history.pushState) {
        history.pushState("", document.title, id);
      }
    }
  });

});



/* -----------------------------------------------
/* Author : Vincent Garreau  - vincentgarreau.com
/* MIT license: http://opensource.org/licenses/MIT
/* Demo / Generator : vincentgarreau.com/particles.js
/* GitHub : github.com/VincentGarreau/particles.js
/* How to use? : Check the GitHub README
/* v2.0.0

/* ----------------------------------------------- */




/* ---------- particles.js functions - start ------------ */

// La instalación de la librería se realizó mediante bower. La siguiente función solo configura las opciones de particles.


particlesJS("particles-js", {
  "particles": {
    "number": {
      "value": 80,
      "density": {
        "enable": true,
        "value_area": 800
      }
    },
    "color": {
      "value": "#ffffff"
    },
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#000000"
      },
      "polygon": {
        "nb_sides": 5
      },
      "image": {
        "src": "img/github.svg",
        "width": 100,
        "height": 100
      }
    },
    "opacity": {
      "value": 0.5,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 2,
        "opacity_min": 0.1,
        "sync": false
      }
    },
    "size": {
      "value": 4,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 40,
        "size_min": 0.1,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 150,
      "color": "#ffffff",
      "opacity": 0.6,
      "width": 1.8
    },
    "move": {
      "enable": true,
      "speed": 6,
      "direction": "none",
      "random": false,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": false,
        "rotateX": 600,
        "rotateY": 1200
      }
    }
  },
  "interactivity": {
    "detect_on": "window",
    "events": {
      "onhover": {
        "enable": false,  //Si se quiere interacción con el mouse pasar a true
        "mode": "repulse"
      },
      "onclick": {
        "enable": false, //Si se quiere interacción con el mouse pasar a true
        "mode": "push"
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 140,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 400,
        "size": 40,
        "duration": 2,
        "opacity": 8,
        "speed": 3
      },
      "repulse": {
        "distance": 150,
        "duration": 0.3
      },
      "push": {
        "particles_nb": 6
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect": true
});







// ----------- Menu ------------------------------

(function (window) {

    'use strict';

    // class helper functions from bonzo https://github.com/ded/bonzo

    function classReg(className) {
        return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
    }

    // classList support for class management
    // altho to be fair, the api sucks because it won't accept multiple classes at once
    var hasClass, addClass, removeClass;

    if ('classList' in document.documentElement) {
        hasClass = function (elem, c) {
            return elem.classList.contains(c);
        };
        addClass = function (elem, c) {
            elem.classList.add(c);
        };
        removeClass = function (elem, c) {
            elem.classList.remove(c);
        };
    }
    else {
        hasClass = function (elem, c) {
            return classReg(c).test(elem.className);
        };
        addClass = function (elem, c) {
            if (!hasClass(elem, c)) {
                elem.className = elem.className + ' ' + c;
            }
        };
        removeClass = function (elem, c) {
            elem.className = elem.className.replace(classReg(c), ' ');
        };
    }

    function toggleClass(elem, c) {
        var fn = hasClass(elem, c) ? removeClass : addClass;
        fn(elem, c);
    }

    var classie = {
        // full names
        hasClass: hasClass,
        addClass: addClass,
        removeClass: removeClass,
        toggleClass: toggleClass,
        // short names
        has: hasClass,
        add: addClass,
        remove: removeClass,
        toggle: toggleClass
    };

    window.classie = classie;

    // transport
    // if (typeof define === 'function' && define.amd) {
    //     // AMD
    //     define(classie);
    // } else {
    //     // browser global
    //     window.classie = classie;
    // }

})(window);

function init() {
    window.addEventListener('scroll', function (e) {
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("header");
        if (distanceY > shrinkOn) {
            classie.add(header, "smaller");
        } else {
            if (classie.has(header, "smaller")) {
                classie.remove(header, "smaller");
            }
        }
    });
}

window.onload = init();


// -----------End Menu ------------------------------

// ----------- Our Work Slider and Client Slider ------------------------------



  var $work_slider = $(".work-slider");

  var work_slider_config = {
      slidesToShow: 2,
      slidesToScroll: 2,
      arrows: true,
      infinite: false,
      dots: true,
      speed: 300,
      useCSS: true,
      responsive: [
        {
          breakpoint: 760,
          settings: {
            slidesToShow: 2,
            arrows: false
          }
        },
        {
          breakpoint: 960,
          settings: {
            slidesToShow: 3,
            arrows: false
          }
        },
        {
          breakpoint: 480,
          settings: "unslick"
        },
      ]
  };


  var $client_slider = $("#client-slider");


    var client_slider_config = {
      slidesToShow: 5,
      slidesToScroll: 2,
      arrows: false,
      infinite: false,
      dots: false,
      speed: 300,
      useCSS: true,
      responsive: [
        {
          breakpoint: 760,
          settings: {
            slidesToShow: 2,
            arrows: false
          }
        },
        {
          breakpoint: 960,
          settings: {
            slidesToShow: 4,
            arrows: false
          }
        },
        {
          breakpoint: 480,
          settings: "unslick"
        },
      ]
    };


 $(document).ready(function(){

    $work_slider.slick(work_slider_config);
    $client_slider.slick(client_slider_config);

    $(window).on('resize', function() {
    if ($(window).width() < 480) {
      if ($work_slider.hasClass('slick-initialized')) {
        $work_slider.slick('unslick');
      }
      if ($client_slider.hasClass('slick-initialized')) {
        $client_slider.slick('unslick');
      }
      return
    }

    if (!$client_slider.hasClass('slick-initialized')) {
      return $client_slider.slick(client_slider_config);
    }
    if (!$work_slider.hasClass('slick-initialized')) {
      return $work_slider.slick(work_slider_config);
    }
  });

// ----------- END Our Work Slider and Client Slider ------------------------------ //


});




// ----------- Incremental counters -------------------------


 (function ($) {
     $.fn.countTo = function (options) {
         // merge the default plugin settings with the custom options
         options = $.extend({}, $.fn.countTo.defaults, options || {});

         // how many times to update the value, and how much to increment the value on each update
         var loops = Math.ceil(options.speed / options.refreshInterval),
             increment = (options.to - options.from) / loops;

         return $(this).each(function () {
             var _this = this,
                 loopCount = 0,
                 value = options.from,
                 interval = setInterval(updateTimer, options.refreshInterval);

             function updateTimer() {
                 value += increment;
                 loopCount++;
                 $(_this).html(value.toFixed(options.decimals));

                 if (typeof (options.onUpdate) == 'function') {
                     options.onUpdate.call(_this, value);
                 }

                 if (loopCount >= loops) {
                     clearInterval(interval);
                     value = options.to;

                     if (typeof (options.onComplete) == 'function') {
                         options.onComplete.call(_this, value);
                     }
                 }
             }
         });
     };

     $.fn.countTo.defaults = {
         from: 0,  // the number the element should start at
         to: 100,  // the number the element should end at
         speed: 1000,  // how long it should take to count between the target numbers
         refreshInterval: 100,  // how often the element should be updated
         decimals: 0,  // the number of decimal places to show
         onUpdate: null,  // callback method for every time the element is updated,
         onComplete: null,  // callback method for when the element finishes updating
     };
 })(jQuery);

 jQuery(function ($) {
     $('.cofeecounter').countTo({
         from: 0,
         to: 6513,
         speed: 10000,
         refreshInterval: 50,
     });
 });

 jQuery(function ($) {
     $('.soccercounter').countTo({
         from: 0,
         to: 482,
         speed: 10000,
         refreshInterval: 50,
     });
 });

 jQuery(function ($) {
     $('.barbacuecounter').countTo({
         from: 0,
         to: 124,
         speed: 10000,
         refreshInterval: 50,
     });
 });

 jQuery(function ($) {
     $('.worldcounter').countTo({
         from: 0,
         to: 2,
         speed: 5000,
         refreshInterval: 50,
     });
 });


// ----------- END Incremental counters -------------------------



// ----------- PINS and their content on TEAM img -------------------------


 $(document).ready(function(){

  $('.pin').easypinShow({

    data: {
        "example_image1":
        {
          "0":{
            "content":"Debora Carrizo",
            "description":"Lorem ipsum dolot sit amet. Lorem ipsum dolot sit amet.",
            "image_url": "assets/img/debo.jpg",
            "title":"Debora Carrizo Proyect Manager",
            "coords":{"lat":"320","long":"30"}
          },
        "1":{
            "content":"Rodrigo Falco",
            "description":"Lorem ipsum dolot sit amet. Lorem ipsum dolot sit amet.",
            "image_url": "assets/img/rodri.jpg",
            "title":"Rodrigo Falco CTO",
            "coords":{"lat":"1038","long":"42"}
          },
        "2":{
            "content":"Ricardo",
            "description":"Lorem ipsum dolot sit amet. Lorem ipsum dolot sit amet.",
            "title":"Ricardo Markiewicz Senior Dev",
            "coords":{"lat":"820","long":"35"}
          },
          "canvas":{
            "src":"img/team1.png",
            "width":"1200",
            "height":"630"
          }
        }
  },
  responsive: true,
  popover: {
        show: false,
        animate: true
  },

});

  $(window).resize(function(){
    var bp = Foundation.MediaQuery.current;
    console.log(bp);
  })

});

// ----------- END PINS -------------------------
